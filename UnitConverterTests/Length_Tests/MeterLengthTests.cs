﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Length;

namespace UnitConverterTests.Length_Tests
{
    [TestClass]
    public class MeterLengthTests
    {
        readonly IConvert _convertUnit = new Meter();

        [TestMethod]
        public void TestMeterToCentimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Centimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 500);
        }

        [TestMethod]
        public void TestMeterToMillimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Millimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 5000);
        }

        [TestMethod]
        public void TestMeterToYard()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Yard", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 5.468);
        }

        [TestMethod]
        public void TestMeterToFoot()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Foot", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 16.4042);
        }

        [TestMethod]
        public void TestMeterToInch()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Inch", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 196.8505);
        }
    }
}
