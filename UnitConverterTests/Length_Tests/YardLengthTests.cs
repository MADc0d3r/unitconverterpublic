﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Length;

namespace UnitConverterTests.Length_Tests
{
    [TestClass]
    public class YardLengthTests
    {
        readonly IConvert _convertUnit = new Yard();

        [TestMethod]
        public void TestYardToMeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Meter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 4.572);
        }

        [TestMethod]
        public void TestYardToCentimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Centimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 457.2);
        }

        [TestMethod]
        public void TestYardToMillimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Millimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 4572);
        }

        [TestMethod]
        public void TestYardToFoot()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Foot", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 15);
        }

        [TestMethod]
        public void TestYardToInch()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Inch", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 180);
        }
    }
}
