﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Length;

namespace UnitConverterTests.Length_Tests
{
    [TestClass]
    public class InchLengthTests
    {
        readonly IConvert _convertUnit = new Inch();

        [TestMethod]
        public void TestInchToMeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Meter", valueToConvert); 

            // Assert
            Assert.AreEqual(convertedValue, 0.1270);
        }

        [TestMethod]
        public void TestInchToCentimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Centimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 12.7000);
        }

        [TestMethod]
        public void TestInchToMillimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Millimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 127.0);
        }

        [TestMethod]
        public void TestInchToYard()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Yard", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1389);
        }

        [TestMethod]
        public void TestInchToFoot()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Foot", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.4167);
        }
    }
}
