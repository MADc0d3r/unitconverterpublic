﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Length;

namespace UnitConverterTests.Length_Tests
{
    [TestClass]
    public class MillimeterLengthTests
    {
        readonly IConvert _convertUnit = new Millimeter();

        [TestMethod]
        public void TestMillimeterToMeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Meter", valueToConvert); 

            // Assert
            Assert.AreEqual(convertedValue, 0.005);
        }

        [TestMethod]
        public void TestMillimeterToCentimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Centimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.5);
        }

        [TestMethod]
        public void TestMillimeterToYard()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Yard", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0055);
        }

        [TestMethod]
        public void TestMillimeterToFoot()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Foot", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0164);
        }

        [TestMethod]
        public void TestMillimeterToInch()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Inch", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1969);
        }
    }
}
