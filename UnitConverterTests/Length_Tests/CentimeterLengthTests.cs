﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Length;

namespace UnitConverterTests.Length_Tests
{
    [TestClass]
    public class CentimeterLengthTests
    {
        readonly IConvert _convertUnit = new Centimeter();

        [TestMethod]
        public void TestCentimeterToMeter()
        {
            // Arrange
            const double valueToConvert = 5.0;
            
            // Act
            double convertedValue = _convertUnit.Convert("Meter", valueToConvert);
            
            // Assert
            Assert.AreEqual(convertedValue, 0.05);
        }

        [TestMethod]
        public void TestCentimeterToMillimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Millimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 50);
        }

        [TestMethod]
        public void TestCentimeterToYard()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Yard", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0547);
        }

        [TestMethod]
        public void TestCentimeterToFoot()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Foot", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1640);
        }

        [TestMethod]
        public void TestCentimeterToInch()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Inch", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.9685);
        }
    }
}
