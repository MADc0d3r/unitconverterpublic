﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Length;

namespace UnitConverterTests.Length_Tests
{
    [TestClass]
    public class FootLengthTests
    {
        readonly IConvert _convertUnit = new Foot();

        [TestMethod]
        public void TestFootToMeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Meter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.524);
        }

        [TestMethod]
        public void TestFootToCentimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Centimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 152.4);
        }

        [TestMethod]
        public void TestFootToMillimeter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Millimeter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1524);
        }

        [TestMethod]
        public void TestFootToYard()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Yard", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.6667);
        }

        [TestMethod]
        public void TestFootToInch()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Inch", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 60.0);
        }
    }
}
