﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Mass;

namespace UnitConverterTests.Mass_Tests
{
    [TestClass]
    public class PoundMassTests
    {
        readonly IConvert _convertUnit = new Pound();

        [TestMethod]
        public void TestPoundToKilogram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Kilogram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2.268);
        }

        [TestMethod]
        public void TestPoundToGram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Gram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2267.96);
        }

        [TestMethod]
        public void TestPoundToMilligram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milligram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2267960.0);
        }

        [TestMethod]
        public void TestPoundToOunce()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Ounce", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 80);
        }
    }
}
