﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Mass;

namespace UnitConverterTests.Mass_Tests
{
    [TestClass]
    public class MilligramMassTests
    {
        readonly IConvert _convertUnit = new Milligram();

        [TestMethod]
        public void TestMilligramToKilogram()
        {
            // Arrange
            const double valueToConvert = 100.0;

            // Act
            double convertedValue = _convertUnit.Convert("Kilogram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0001);
        }

        [TestMethod]
        public void TestMilligramToGram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Gram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.005);
        }

        [TestMethod]
        public void TestMilligramToPound()
        {
            // Arrange
            const double valueToConvert = 1000.0;

            // Act
            double convertedValue = _convertUnit.Convert("Pound", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0022);
        }

        [TestMethod]
        public void TestMilligramToOunce()
        {
            // Arrange
            const double valueToConvert = 1000;

            // Act
            double convertedValue = _convertUnit.Convert("Ounce", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0353);
        }
    }
}
