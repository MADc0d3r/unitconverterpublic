﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Mass;

namespace UnitConverterTests.Mass_Tests
{
    [TestClass]
    public class GramMassTests
    {
        readonly IConvert _convertUnit = new Gram();

        [TestMethod]
        public void TestGramToKilogram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Kilogram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.005);
        }

        [TestMethod]
        public void TestGramToMilligram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milligram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 5000);
        }

        [TestMethod]
        public void TestGramToPound()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Pound", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0110);
        }

        [TestMethod]
        public void TestGramToOunce()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Ounce", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1764);
        }
    }
}
