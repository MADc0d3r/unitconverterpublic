﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Mass;

namespace UnitConverterTests.Mass_Tests
{
    [TestClass]
    public class OunceMassTests
    {
        readonly IConvert _convertUnit = new Ounce();

        [TestMethod]
        public void TestOunceToKilogram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Kilogram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1417);
        }

        [TestMethod]
        public void TestOunceToGram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Gram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 141.7475);
        }

        [TestMethod]
        public void TestOunceToMilligram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milligram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 141747.5);
        }

        [TestMethod]
        public void TestOunceToPound()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Pound", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.3125);
        }
    }
}
