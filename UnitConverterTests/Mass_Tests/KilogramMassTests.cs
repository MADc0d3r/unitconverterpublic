﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Mass;

namespace UnitConverterTests.Mass_Tests
{
    [TestClass]
    public class KilogramMassTests
    {
        readonly IConvert _convertUnit = new Kilogram();

        [TestMethod]
        public void TestKilogramToGram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Gram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 5000);
        }

        [TestMethod]
        public void TestKilogramToMilligram()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milligram", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 5000000.0);
        }

        [TestMethod]
        public void TestKilogramToPound()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Pound", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 11.0231);
        }

        [TestMethod]
        public void TestKilogramToOunce()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Ounce", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 176.37);
        }
    }
}
