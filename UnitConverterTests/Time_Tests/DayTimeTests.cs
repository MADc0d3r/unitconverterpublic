﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Time;

namespace UnitConverterTests.Time_Tests
{
    [TestClass]
    public class DayTimeTests
    {
        readonly IConvert _convertUnit = new Day();

        [TestMethod]
        public void TestDayToSecond()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Second", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 432000);
        }

        [TestMethod]
        public void TestDayToMinute()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Minute", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 7200);
        }

        [TestMethod]
        public void TestDayToHour()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Hour", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 120);
        }
    }
}
