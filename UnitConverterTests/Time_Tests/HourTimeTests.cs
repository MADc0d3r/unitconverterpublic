﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Time;

namespace UnitConverterTests.Time_Tests
{
    [TestClass]
    public class HourTimeTests
    {
        readonly IConvert _convertUnit = new Hour();

        [TestMethod]
        public void TestHourToSecond()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Second", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 18000);
        }

        [TestMethod]
        public void TestHourToMinute()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Minute", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 300);
        }

        [TestMethod]
        public void TestHourToDay()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Day", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.2083);
        }
    }
}
