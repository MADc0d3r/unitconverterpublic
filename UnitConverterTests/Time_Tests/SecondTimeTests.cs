﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Time;

namespace UnitConverterTests.Time_Tests
{
    [TestClass]
    public class SecondTimeTests
    {
        readonly IConvert _convertUnit = new Second();

        [TestMethod]
        public void TestSecondToMinute()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Minute", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0833);
        }

        [TestMethod]
        public void TestSecondToHour()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Hour", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0014);
        }

        [TestMethod]
        public void TestSecondToDay()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Day", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0001);
        }
    }
}
