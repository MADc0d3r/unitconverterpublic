﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Time;

namespace UnitConverterTests.Time_Tests
{
    [TestClass]
    public class MinuteTimeTests
    {
        readonly IConvert _convertUnit = new Minute();

        [TestMethod]
        public void TestMinuteToSecond()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Second", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 300);
        }

        [TestMethod]
        public void TestMinuteToHour()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Hour", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0833);
        }

        [TestMethod]
        public void TestMinuteToDay()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Day", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0035);
        }
    }
}
