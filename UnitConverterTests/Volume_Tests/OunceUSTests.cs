﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Volume;

namespace UnitConverterTests.Volume_Tests
{
    [TestClass]
    public class OunceUSTests
    {
        private readonly IConvert _convertUnit = new OunceUS();

        [TestMethod]
        public void TestOunceUSToGallonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0391);
        }

        [TestMethod]
        public void TestOunceUSToQuartUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1562);
        }

        [TestMethod]
        public void TestOunceUSToPintUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.3125);
        }

        [TestMethod]
        public void TestOunceUSToCupUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("CupUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.625);
        }

        [TestMethod]
        public void TestOunceUSToTablespoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 10);
        }

        [TestMethod]
        public void TestOunceUSToTeaspoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 30);
        }

        [TestMethod]
        public void TestOunceUSToLiter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Liter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1479);
        }

        [TestMethod]
        public void TestOunceUSToMilliliter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milliliter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 147.8675);
        }

        [TestMethod]
        public void TestOunceUSToGallonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0325);
        }

        [TestMethod]
        public void TestOunceUSToQuartImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1301);
        }

        [TestMethod]
        public void TestOunceUSToPintImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.2602);
        }

        [TestMethod]
        public void TestOunceUSToTablespoonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 8.3268);
        }

        [TestMethod]
        public void TestOunceUSToTeaspoonImperial()
        {
            // Arrange
            const double valueToConvert = 5;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 24.9802);
        }
    }
}
