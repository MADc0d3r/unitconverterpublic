﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Volume;

namespace UnitConverterTests.Volume_Tests
{
    [TestClass]
    public class PintUSTests
    {
        private readonly IConvert _convertUnit = new PintUS();

        [TestMethod]
        public void TestPintUSToGallonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.625);
        }

        [TestMethod]
        public void TestPintUSToQuartUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2.5);
        }

        [TestMethod]
        public void TestPintUSToCupUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("CupUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 10);
        }

        [TestMethod]
        public void TestPintUSToOunceUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 80);
        }

        [TestMethod]
        public void TestPintUSToTablespoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 160);
        }

        [TestMethod]
        public void TestPintUSToTeaspoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 480);
        }

        [TestMethod]
        public void TestPintUSToLiter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Liter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2.3659);
        }

        [TestMethod]
        public void TestPintUSToMilliliter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milliliter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2365.88);
        }

        [TestMethod]
        public void TestPintUSToGallonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.5204);
        }

        [TestMethod]
        public void TestPintUSToQuartImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2.0817);
        }

        [TestMethod]
        public void TestPintUSToPintImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 4.1634);
        }

        [TestMethod]
        public void TestPintUSToOunceImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 83.2675);
        }

        [TestMethod]
        public void TestPintUSToTablespoonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 133.228);
        }

        [TestMethod]
        public void TestPintUSToTeaspoonImperial()
        {
            // Arrange
            const double valueToConvert = 5;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 399.6835);
        }
    }
}
