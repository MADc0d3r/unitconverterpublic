﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Volume;

namespace UnitConverterTests.Volume_Tests
{
    [TestClass]
    public class LiterTests
    {
        private readonly IConvert _convertUnit = new Liter();

        [TestMethod]
        public void TestLiterToGallonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.3209);
        }

        [TestMethod]
        public void TestLiterToQuartUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 5.2834);
        }

        [TestMethod]
        public void TestLiterToPintUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 10.5669);
        }

        [TestMethod]
        public void TestLiterToCupUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("CupUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 21.1338);
        }

        [TestMethod]
        public void TestLiterToOunceUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 169.07);
        }

        [TestMethod]
        public void TestLiterToTablespoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 338.14);
        }

        [TestMethod]
        public void TestLiterToTeaspoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1014.42);
        }

        [TestMethod]
        public void TestLiterToMilliliter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milliliter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 5000);
        }

        [TestMethod]
        public void TestLiterToGallonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.0998);
        }

        [TestMethod]
        public void TestLiterToQuartImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 4.3994);
        }

        [TestMethod]
        public void TestLiterToPintImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 8.7988);
        }

        [TestMethod]
        public void TestLiterToOunceImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 175.9755);
        }

        [TestMethod]
        public void TestLiterToTablespoonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 281.5605);
        }

        [TestMethod]
        public void TestLiterToTeaspoonImperial()
        {
            // Arrange
            const double valueToConvert = 5;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 844.68);
        }

    }
}
