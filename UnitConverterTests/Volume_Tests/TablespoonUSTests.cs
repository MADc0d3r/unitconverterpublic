﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Volume;

namespace UnitConverterTests.Volume_Tests
{
    [TestClass]
    public class TablespoonUSTests
    {
        readonly IConvert _convertUnit = new TablespoonUS();

        [TestMethod]
        public void TestTablespoonUSToGallonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0195);
        }

        [TestMethod]
        public void TestTablespoonUSToQuartUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0781);
        }

        [TestMethod]
        public void TestTablespoonUSToPintUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1562);
        }

        [TestMethod]
        public void TestTablespoonUSToCupUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("CupUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.3125);
        }

        [TestMethod]
        public void TestTablespoonUSToOunceUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2.5);
        }

        [TestMethod]
        public void TestTablespoonUSToTeaspoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 15);
        }

        [TestMethod]
        public void TestTablespoonUSToLiter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Liter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0739);
        }

        [TestMethod]
        public void TestTablespoonUSToMilliliter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milliliter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 73.934);
        }

        [TestMethod]
        public void TestTablespoonUSToGallonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0163);
        }

        [TestMethod]
        public void TestTablespoonUSToQuartImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0651);
        }

        [TestMethod]
        public void TestTablespoonUSToPintImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1301);
        }

        [TestMethod]
        public void TestTablespoonUSToOunceImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2.6021);
        }

        [TestMethod]
        public void TestTablespoonUSToTablespoonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 4.1634);
        }
        
        [TestMethod]
        public void TestTablespoonUSToTeaspoonImperial()
        {
            // Arrange
            const double valueToConvert = 5;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 12.4901);
        }
    }
}
