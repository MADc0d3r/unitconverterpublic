﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Volume;

namespace UnitConverterTests.Volume_Tests
{
    [TestClass]
    public class TeaspoonImperialTests
    {
        readonly IConvert _convertUnit = new TeaspoonImperial();

        [TestMethod]
        public void TestTeaspoonImperialToGallonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0078);
        }

        [TestMethod]
        public void TestTeaspoonImperialToQuartUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0313);
        }

        [TestMethod]
        public void TestTeaspoonImperialToPintUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0625);
        }

        [TestMethod]
        public void TestTeaspoonImperialToCupUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("CupUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1251);
        }

        [TestMethod]
        public void TestTeaspoonImperialToOunceUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.0008);
        }

        [TestMethod]
        public void TestTeaspoonImperialToTablespoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 2.0016);
        }

        [TestMethod]
        public void TestTeaspoonImperialToTeaspoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 6.0047);
        }

        [TestMethod]
        public void TestTeaspoonImperialToLiter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Liter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0296);
        }

        [TestMethod]
        public void TestTeaspoonImperialToMilliliter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Milliliter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 29.597);
        }

        [TestMethod]
        public void TestTeaspoonImperialToGallonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0065);
        }

        [TestMethod]
        public void TestTeaspoonImperialToQuartImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0260);
        }

        [TestMethod]
        public void TestTeaspoonImperialToPintImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0521);
        }

        [TestMethod]
        public void TestTeaspoonImperialToOunceImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.0417);
        }

        [TestMethod]
        public void TestTeaspoonImperialToTablespoonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.6667);
        }

    }
}
