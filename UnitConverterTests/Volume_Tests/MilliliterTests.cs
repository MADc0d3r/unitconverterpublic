﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Volume;

namespace UnitConverterTests.Volume_Tests
{
    [TestClass]
    public class MilliliterTests
    {
        private readonly IConvert _convertUnit = new Milliliter();

        [TestMethod]
        public void TestMilliliterToGallonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0013);
        }

        [TestMethod]
        public void TestMilliliterToQuartUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0053);
        }

        [TestMethod]
        public void TestMilliliterToPintUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0106);
        }

        [TestMethod]
        public void TestMilliliterToCupUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("CupUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0211);
        }

        [TestMethod]
        public void TestMilliliterToOunceUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.1691);
        }

        [TestMethod]
        public void TestMilliliterToTablespoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.3381);
        }

        [TestMethod]
        public void TestMilliliterToTeaspoonUS()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonUS", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 1.0144);
        }

        [TestMethod]
        public void TestMilliliterToLiter()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Liter", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.005);
        }

        [TestMethod]
        public void TestMilliliterToGallonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("GallonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0011);
        }

        [TestMethod]
        public void TestMilliliterToQuartImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("QuartImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0044);
        }

        [TestMethod]
        public void TestMilliliterToPintImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("PintImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.0088);
        }

        [TestMethod]
        public void TestMilliliterToOunceImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("OunceImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.176);
        }

        [TestMethod]
        public void TestMilliliterToTablespoonImperial()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("TablespoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.2816);
        }

        [TestMethod]
        public void TestMilliliterToTeaspoonImperial()
        {
            // Arrange
            const double valueToConvert = 5;

            // Act
            double convertedValue = _convertUnit.Convert("TeaspoonImperial", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 0.8447);
        }

    }
}
