﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Temperature;

namespace UnitConverterTests.Temperature_Tests
{

    [TestClass]
    public class FahrenheitTemperatureTests
    {
        readonly IConvert _convertUnit = new Fahrenheit();

        [TestMethod]
        public void TestFahrenheitToCelsius()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Celsius", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, -15);
        }

        [TestMethod]
        public void TestFahrenheitToKelvin()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Kelvin", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 258.15);
        }
    }
}
