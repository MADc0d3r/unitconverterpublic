﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Temperature;

namespace UnitConverterTests.Temperature_Tests
{
    [TestClass]
    public class KelvinTemperatureTests
    {
        readonly IConvert _convertUnit = new Kelvin();

        [TestMethod]
        public void TestKelvinToCelsius()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Celsius", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, -268.15);
        }

        [TestMethod]
        public void TestKelvinToFahrenheit()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Fahrenheit", valueToConvert);

            // Assert
            Assert.AreEqual(Math.Round(convertedValue,2), -450.67);
        }
    }
}
