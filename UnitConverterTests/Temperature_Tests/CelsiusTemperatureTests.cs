﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitConverter;
using UnitConverter.Temperature;

namespace UnitConverterTests.Temperature_Tests
{
    [TestClass]
    public class CelsiusTemperatureTests
    {
        readonly IConvert _convertUnit = new Celsius();

        [TestMethod]
        public void TestCelsiusToFahrenheit()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Fahrenheit", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 41);
        }

        [TestMethod]
        public void TestCelsiusToKelvin()
        {
            // Arrange
            const double valueToConvert = 5.0;

            // Act
            double convertedValue = _convertUnit.Convert("Kelvin", valueToConvert);

            // Assert
            Assert.AreEqual(convertedValue, 278.15);
        }
    }
}
