﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Temperature
// Class:          Kelvin
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Temperature
{
    /// <summary>
    /// Kelvin conversion methods.
    /// </summary>
    public class Kelvin : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Celsius":
                    aConvertedValue = ToCelsius(aValueToConvert);
                    break;
                case "Fahrenheit":
                    aConvertedValue = ToFahrenheit(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts a Kelvin to Celsius.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Celsius converted to Celsius.</returns>
        public static double ToCelsius(double aValueToConvert)
        {
            return aValueToConvert - 273.15;
        }

        /// <summary>
        /// Converts a Kelvin to Fahrenheit.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Celsius converted to Fahrenheit.</returns>
        public static double ToFahrenheit(double aValueToConvert)
        {
            return ((aValueToConvert - 273.15) * 1.8) + 32;
        }
    }
}
