﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Temperature
// Class:          Celsius
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Temperature
{
    /// <summary>
    /// Celsius conversion methods.
    /// </summary>
    public class Celsius : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Fahrenheit":
                    aConvertedValue = ToFahrenheit(aValueToConvert);
                    break;
                case "Kelvin":
                    aConvertedValue = ToKelvin(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts a Celsius to Kelvin.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Celsius converted to Kelvin.</returns>
        public static double ToKelvin(double aValueToConvert)
        {
            return aValueToConvert + 273.15 ;
        }

        /// <summary>
        /// Converts a Celsius to Fahrenheit.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Celsius converted to Fahrenheit.</returns>
        public static double ToFahrenheit(double aValueToConvert)
        {
            return ((9.0 / 5.0) * aValueToConvert) + 32;
        }
    }
}
