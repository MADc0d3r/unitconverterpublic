﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Temperature
// Class:          Fahrenheit
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Temperature
{
    /// <summary>
    /// Fahrenheit conversion methods.
    /// </summary>
    public class Fahrenheit : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Celsius":
                    aConvertedValue = ToCelsius(aValueToConvert);
                    break;
                case "Kelvin":
                    aConvertedValue = ToKelvin(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }
        /// <summary>
        /// Converts a Fahrenheit to Kelvin.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Fahrenheit converted to Kelvin.</returns>
        public static double ToKelvin(double aValueToConvert)
        {
            return (((aValueToConvert - 32) / 1.8) + 273.15);
        }

        /// <summary>
        /// Converts a Fahrenheit to Celsius.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Fahrenheit converted to Celsius.</returns>
        public static double ToCelsius(double aValueToConvert)
        {
            return (aValueToConvert - 32) * (5.0 / 9.0);
        }
    }
}
