﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter
// Class:          IConvert
// Description:    Conversion Utility Interface.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/13/2015   Initial Design 
//-----------------------------------------------------------------

namespace UnitConverter
{
    /// <summary>
    /// Public interface used for unit conversion.
    /// </summary>
    public interface IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        double Convert(string aUnitToConvertTo, double aValueToConvert);
    }
}
