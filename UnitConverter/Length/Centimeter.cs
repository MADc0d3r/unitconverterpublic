﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Length
// Class:          Centimeter
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/06/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Length
{
    /// <summary>
    /// Centimeter conversion methods.
    /// </summary>
    public class Centimeter : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Meter":
                    aConvertedValue = ToMeter(aValueToConvert);
                    break;
                case "Millimeter":
                    aConvertedValue = ToMillimeter(aValueToConvert);
                    break;
                case "Yard":
                    aConvertedValue = ToYard(aValueToConvert);
                    break;
                case "Foot":
                    aConvertedValue = ToFoot(aValueToConvert);
                    break;
                case "Inch":
                    aConvertedValue = ToInch(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Centimeter to Meter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Centimeter converted to Meter.</returns>
        public static double ToMeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.01, 4);
        }
        
        /// <summary>
        /// Converts Centimeter to Millimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Centimeter converted to Millimeter.</returns>
        public static double ToMillimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 10, 4);
        }

        /// <summary>
        /// Converts Centimeter to Yard.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Centimeter converted to Yard.</returns>
        public static double ToYard(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0109361, 4);
        }

        /// <summary>
        /// Converts Centimeter to Foot.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Centimeter converted to Foot.</returns>
        public static double ToFoot(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0328084, 4);
        }

        /// <summary>
        /// Converts Centimeter to Inch.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Centimeter converted to Inch.</returns>
        public static double ToInch(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.393701, 4);
        }
    }
}
