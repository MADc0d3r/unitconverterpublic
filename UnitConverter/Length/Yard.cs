﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Length
// Class:          Yard
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/06/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Length
{
    /// <summary>
    /// Yard conversion methods.
    /// </summary>
    public class Yard : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Meter":
                    aConvertedValue = ToMeter(aValueToConvert);
                    break;
                case "Centimeter":
                    aConvertedValue = ToCentimeter(aValueToConvert);
                    break;
                case "Millimeter":
                    aConvertedValue = ToMillimeter(aValueToConvert);
                    break;
                case "Foot":
                    aConvertedValue = ToFoot(aValueToConvert);
                    break;
                case "Inch":
                    aConvertedValue = ToInch(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Yard to Meter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Yard converted to Meter.</returns>
        public static double ToMeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.9144, 4);
        }

        /// <summary>
        /// Converts Yard to Centimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Yard converted to Centimeter.</returns>
        public static double ToCentimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 91.44, 4);
        }

        /// <summary>
        /// Converts Yard to Millimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Yard converted to Millimeter.</returns>
        public static double ToMillimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 914.4, 4);
        }

        /// <summary>
        /// Converts Yard to Foot.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Yard converted to Foot.</returns>
        public static double ToFoot(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3, 4);
        }

        /// <summary>
        /// Converts Yard to Inch.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Yard converted to Inch.</returns>
        public static double ToInch(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 36, 4);
        }
    }
}
