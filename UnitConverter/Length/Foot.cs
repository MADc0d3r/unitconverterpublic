﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Length
// Class:          Foot
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/06/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Length
{
    /// <summary>
    /// Foot conversion methods.
    /// </summary>
    public class Foot : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Meter":
                    aConvertedValue = ToMeter(aValueToConvert);
                    break;
                case "Centimeter":
                    aConvertedValue = ToCentimeter(aValueToConvert);
                    break;
                case "Millimeter":
                    aConvertedValue = ToMillimeter(aValueToConvert);
                    break;
                case "Yard":
                    aConvertedValue = ToYard(aValueToConvert);
                    break;
                case "Inch":
                    aConvertedValue = ToInch(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Foot to Meter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Foot converted to Meter.</returns>
        public static double ToMeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.3048, 4);
        }

        /// <summary>
        /// Converts Foot to Centimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Foot converted to Centimeter.</returns>
        public static double ToCentimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 30.48, 4);
        }

        /// <summary>
        /// Converts Foot to Millimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Foot converted to Millimeter.</returns>
        public static double ToMillimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 304.8, 4);
        }

        /// <summary>
        /// Converts Foot to Yard.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Foot converted to Yard.</returns>
        public static double ToYard(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.333333, 4);
        }

        /// <summary>
        /// Converts Foot to Inch.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Foot converted to Inch.</returns>
        public static double ToInch(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 12, 4);
        }
    }
}
