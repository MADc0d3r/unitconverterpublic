﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Length
// Class:          Millimeter
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/06/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Length
{
    /// <summary>
    /// Millimeter conversion methods.
    /// </summary>
    public class Millimeter : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Meter":
                    aConvertedValue = ToMeter(aValueToConvert);
                    break;
                case "Centimeter":
                    aConvertedValue = ToCentimeter(aValueToConvert);
                    break;
                case "Yard":
                    aConvertedValue = ToYard(aValueToConvert);
                    break;
                case "Foot":
                    aConvertedValue = ToFoot(aValueToConvert);
                    break;
                case "Inch":
                    aConvertedValue = ToInch(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Millimeter to Meter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Millimeter converted to Meter.</returns>
        public static double ToMeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.001, 4);
        }

        /// <summary>
        /// Converts Millimeter to Centimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Millimeter converted to Centimeter.</returns>
        public static double ToCentimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.1, 4);
        }

        /// <summary>
        /// Converts Millimeter to Yard.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Millimeter converted to Yard.</returns>
        public static double ToYard(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.00109361, 4);
        }

        /// <summary>
        /// Converts Millimeter to Foot.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Millimeter converted to Foot.</returns>
        public static double ToFoot(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.00328084, 4);
        }

        /// <summary>
        /// Converts Millimeter to Inch.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Millimeter converted to Inch.</returns>
        public static double ToInch(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0393701, 4);
        }
    }
}
