﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Length
// Class:          Meter
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/06/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Length
{
    /// <summary>
    /// Meter conversion methods.
    /// </summary>
    public class Meter : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Centimeter":
                    aConvertedValue = ToCentimeter(aValueToConvert);
                    break;
                case "Millimeter":
                    aConvertedValue = ToMillimeter(aValueToConvert);
                    break;
                case "Yard":
                    aConvertedValue = ToYard(aValueToConvert);
                    break;
                case "Foot":
                    aConvertedValue = ToFoot(aValueToConvert);
                    break;
                case "Inch":
                    aConvertedValue = ToInch(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Meter to Centimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Meter converted to Centimeter.</returns>
        public static double ToCentimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 100, 4);
        }

        /// <summary>
        /// Converts Meter to Millimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Meter converted to Millimeter.</returns>
        public static double ToMillimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1000, 4);
        }

        /// <summary>
        /// Converts Meter to Yard.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Meter converted to Yard.</returns>
        public static double ToYard(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1.09361, 4);
        }

        /// <summary>
        /// Converts Meter to Foot.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Meter converted to Foot.</returns>
        public static double ToFoot(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3.28084, 4);
        }

        /// <summary>
        /// Converts Meter to Inch.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Meter converted to Inch.</returns>
        public static double ToInch(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 39.3701, 4);
        }
    }
}
