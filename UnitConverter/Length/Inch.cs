﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Length
// Class:          Inch
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/06/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Length
{
    /// <summary>
    /// Inch conversion methods.
    /// </summary>
    public class Inch : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Meter":
                    aConvertedValue = ToMeter(aValueToConvert);
                    break;
                case "Centimeter":
                    aConvertedValue = ToCentimeter(aValueToConvert);
                    break;
                case "Millimeter":
                    aConvertedValue = ToMillimeter(aValueToConvert);
                    break;
                case "Yard":
                    aConvertedValue = ToYard(aValueToConvert);
                    break;
                case "Foot":
                    aConvertedValue = ToFoot(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Inch to Meter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Inch converted to Meter.</returns>
        public static double ToMeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0254, 4);
        }

        /// <summary>
        /// Converts Inch to Centimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Inch converted to Centimeter.</returns>
        public static double ToCentimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 2.54, 4);
        }

        /// <summary>
        /// Converts Inch to Millimeter.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Inch converted to Millimeter.</returns>
        public static double ToMillimeter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 25.4, 4);
        }

        /// <summary>
        /// Converts Inch to Yard.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Inch converted to Yard.</returns>
        public static double ToYard(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0277778, 4);
        }

        /// <summary>
        /// Converts Inch to Foot.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Inch converted to Foot.</returns>
        public static double ToFoot(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0833333, 4);
        }
    }
}
