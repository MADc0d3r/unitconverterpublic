﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Time
// Class:          Day
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/07/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Time
{
    /// <summary>
    /// Day conversion methods.
    /// </summary>
    public class Day : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Second":
                    aConvertedValue = ToSecond(aValueToConvert);
                    break;
                case "Minute":
                    aConvertedValue = ToMinute(aValueToConvert);
                    break;
                case "Hour":
                    aConvertedValue = ToHour(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Second to Second.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Second converted to Second.</returns>
        public static double ToSecond(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 86400, 4);
        }

        /// <summary>
        /// Converts Second to Minute.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Second converted to Minute.</returns>
        public static double ToMinute(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1440, 4);
        }

        /// <summary>
        /// Converts Second to Hour.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Second converted to Hour.</returns>
        public static double ToHour(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 24, 4);
        }

    }
}
