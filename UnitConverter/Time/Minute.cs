﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Time
// Class:          Minute
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/07/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Time
{
    /// <summary>
    /// Minute conversion methods.
    /// </summary>
    public class Minute : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Second":
                    aConvertedValue = ToSecond(aValueToConvert);
                    break;
                case "Hour":
                    aConvertedValue = ToHour(aValueToConvert);
                    break;
                case "Day":
                    aConvertedValue = ToDay(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Second to Second.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Second converted to Second.</returns>
        public static double ToSecond(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 60, 4);
        }

        /// <summary>
        /// Converts Second to Hour.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Second converted to Hour.</returns>
        public static double ToHour(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0166667, 4);
        }

        /// <summary>
        /// Converts Second to Day.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Second converted to Day.</returns>
        public static double ToDay(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.000694444, 4);
        }
    }
}
