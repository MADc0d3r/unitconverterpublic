﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Volume
// Class:          TablespoonUS
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Volume
{
    /// <summary>
    /// Conversions for US Tablespoon.
    /// </summary>
    public class TablespoonUS : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "GallonUS":
                    aConvertedValue = ToGallonUS(aValueToConvert);
                    break;
                case "QuartUS":
                    aConvertedValue = ToQuartUS(aValueToConvert);
                    break;
                case "PintUS":
                    aConvertedValue = ToPintUS(aValueToConvert);
                    break;
                case "CupUS":
                    aConvertedValue = ToCupUS(aValueToConvert);
                    break;
                case "OunceUS":
                    aConvertedValue = ToOunceUS(aValueToConvert);
                    break;
                case "TeaspoonUS":
                    aConvertedValue = ToTeaspoonUS(aValueToConvert);
                    break;
                case "Liter":
                    aConvertedValue = ToLiter(aValueToConvert);
                    break;
                case "Milliliter":
                    aConvertedValue = ToMilliliter(aValueToConvert);
                    break;
                case "GallonImperial":
                    aConvertedValue = ToGallonImperial(aValueToConvert);
                    break;
                case "QuartImperial":
                    aConvertedValue = ToQuartImperial(aValueToConvert);
                    break;
                case "PintImperial":
                    aConvertedValue = ToPintImperial(aValueToConvert);
                    break;
                case "OunceImperial":
                    aConvertedValue = ToOunceImperial(aValueToConvert);
                    break;
                case "TablespoonImperial":
                    aConvertedValue = ToTablespoonImperial(aValueToConvert);
                    break;
                case "TeaspoonImperial":
                    aConvertedValue = ToTeaspoonImperial(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts a US Tablespoon to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to US Gallon.</returns>
        public static double ToGallonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.00390625, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to US Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to US Quart.</returns>
        public static double ToQuartUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.015625, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to US Pint
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to US Pint.</returns>
        public static double ToPintUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.03125, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to US Cup
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to US Cup.</returns>
        public static double ToCupUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0625, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to US Ounce
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to US Ounce.</returns>
        public static double ToOunceUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.5, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to US Teaspoon.</returns>
        public static double ToTeaspoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Liter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Liter.</returns>
        public static double ToLiter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0147868, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Milliliter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Milliliter.</returns>
        public static double ToMilliliter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 14.7868, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Imperial Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Imperial Gallon.</returns>
        public static double ToGallonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.00325263, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Imperial Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Imperial Quart.</returns>
        public static double ToQuartImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0130105, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Imperial Pint.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Imperial Pint.</returns>
        public static double ToPintImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0260211, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Imperial Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Imperial Ounce.</returns>
        public static double ToOunceImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.520421, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Imperial Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Imperial Tablespoon.</returns>
        public static double ToTablespoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.832674, 4);
        }

        /// <summary>
        /// Converts a US Tablespoon to Imperial Teaspoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Tablespoon converted to Imperial Teaspoon.</returns>
        public static double ToTeaspoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 2.49802, 4);
        }
    }
}
