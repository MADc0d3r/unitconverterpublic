﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Volume
// Class:          TablespoonImperial
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Volume
{
    /// <summary>
    /// Conversions for Imperial Tablespoon.
    /// </summary>
    public class TablespoonImperial : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "GallonUS":
                    aConvertedValue = ToGallonUS(aValueToConvert);
                    break;
                case "QuartUS":
                    aConvertedValue = ToQuartUS(aValueToConvert);
                    break;
                case "PintUS":
                    aConvertedValue = ToPintUS(aValueToConvert);
                    break;
                case "CupUS":
                    aConvertedValue = ToCupUS(aValueToConvert);
                    break;
                case "OunceUS":
                    aConvertedValue = ToOunceUS(aValueToConvert);
                    break;
                case "TablespoonUS":
                    aConvertedValue = ToTablespoonUS(aValueToConvert);
                    break;
                case "TeaspoonUS":
                    aConvertedValue = ToTeaspoonUS(aValueToConvert);
                    break;
                case "Liter":
                    aConvertedValue = ToLiter(aValueToConvert);
                    break;
                case "Milliliter":
                    aConvertedValue = ToMilliliter(aValueToConvert);
                    break;
                case "GallonImperial":
                    aConvertedValue = ToGallonImperial(aValueToConvert);
                    break;
                case "QuartImperial":
                    aConvertedValue = ToQuartImperial(aValueToConvert);
                    break;
                case "PintImperial":
                    aConvertedValue = ToPintImperial(aValueToConvert);
                    break;
                case "OunceImperial":
                    aConvertedValue = ToOunceImperial(aValueToConvert);
                    break;
                case "TeaspoonImperial":
                    aConvertedValue = ToTeaspoonImperial(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to US Gallon.</returns>
        public static double ToGallonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.00469121, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to US Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to US Quart.</returns>
        public static double ToQuartUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0187649, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to US Pint
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to US Pint.</returns>
        public static double ToPintUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0375297, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to US Cup
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to US Cup.</returns>
        public static double ToCupUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0750594, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to US Ounce
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to US Ounce.</returns>
        public static double ToOunceUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.600475, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to US Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to US Tablespoon.</returns>
        public static double ToTablespoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1.20095, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to US Teaspoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to US Teaspoon.</returns>
        public static double ToTeaspoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3.60285, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to Liter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to Liter.</returns>
        public static double ToLiter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0177582, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to Milliliter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to Milliliter.</returns>
        public static double ToMilliliter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 17.7582, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to Imperial Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to Imperial Gallon.</returns>
        public static double ToGallonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.00390625, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to Imperial Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to Imperial Quart.</returns>
        public static double ToQuartImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.015625, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to Imperial Pint.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to Imperial Pint.</returns>
        public static double ToPintImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.03125, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to Imperial Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to Imperial Ounce.</returns>
        public static double ToOunceImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.625, 4);
        }

        /// <summary>
        /// Converts an Imperial Tablespoon to Imperial Teaspoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>An Imperial Tablespoon converted to Imperial Teaspoon.</returns>
        public static double ToTeaspoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3, 4);
        }
    }
}
