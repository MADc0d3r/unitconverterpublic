﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Volume
// Class:          Liter
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Volume
{
    /// <summary>
    /// Conversions for Liter.
    /// </summary>
    public class Liter : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "GallonUS":
                    aConvertedValue = ToGallonUS(aValueToConvert);
                    break;
                case "QuartUS":
                    aConvertedValue = ToQuartUS(aValueToConvert);
                    break;
                case "PintUS":
                    aConvertedValue = ToPintUS(aValueToConvert);
                    break;
                case "CupUS":
                    aConvertedValue = ToCupUS(aValueToConvert);
                    break;
                case "OunceUS":
                    aConvertedValue = ToOunceUS(aValueToConvert);
                    break;
                case "TablespoonUS":
                    aConvertedValue = ToTablespoonUS(aValueToConvert);
                    break;
                case "TeaspoonUS":
                    aConvertedValue = ToTeaspoonUS(aValueToConvert);
                    break;
                case "Milliliter":
                    aConvertedValue = ToMilliliter(aValueToConvert);
                    break;
                case "GallonImperial":
                    aConvertedValue = ToGallonImperial(aValueToConvert);
                    break;
                case "QuartImperial":
                    aConvertedValue = ToQuartImperial(aValueToConvert);
                    break;
                case "PintImperial":
                    aConvertedValue = ToPintImperial(aValueToConvert);
                    break;
                case "OunceImperial":
                    aConvertedValue = ToOunceImperial(aValueToConvert);
                    break;
                case "TablespoonImperial":
                    aConvertedValue = ToTablespoonImperial(aValueToConvert);
                    break;
                case "TeaspoonImperial":
                    aConvertedValue = ToTeaspoonImperial(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts a Liter to US Gallon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Liter.</returns>
        public static double ToGallonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.264172, 4);
        }

        /// <summary>
        /// Converts a Liter to US Quart.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to US Quart.</returns>
        public static double ToQuartUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1.05669, 4);
        }

        /// <summary>
        /// Converts a Liter to US Pint.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to US Pint.</returns>
        public static double ToPintUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 2.11338, 4);
        }

        /// <summary>
        /// Converts a Liter to US Cup.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to US Cup.</returns>
        public static double ToCupUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 4.22675, 4);
        }

        /// <summary>
        /// Converts a Liter to US Ounce
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to US Ounce.</returns>
        public static double ToOunceUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 33.814, 4);
        }

        /// <summary>
        /// Converts a Liter to US Tablespoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to US Tablespoon.</returns>
        public static double ToTablespoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 67.628, 4);
        }

        /// <summary>
        /// Converts a Liter to US Teaspoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to US Teaspoon.</returns>
        public static double ToTeaspoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 202.884, 4);
        }

        /// <summary>
        /// Converts a Liter to Milliliter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Milliliter.</returns>
        public static double ToMilliliter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1000, 4);
        }

        /// <summary>
        /// Converts a Liter to Imperial Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Imperial Gallon.</returns>
        public static double ToGallonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.219969, 4);
        }

        /// <summary>
        /// Converts a Liter to Imperial Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Imperial Quart.</returns>
        public static double ToQuartImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.879877, 4);
        }

        /// <summary>
        /// Converts a Liter to Imperial Pint.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Imperial Pint.</returns>
        public static double ToPintImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1.75975, 4);
        }

        /// <summary>
        /// Converts a Liter to Imperial Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Imperial Ounce.</returns>
        public static double ToOunceImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 35.1951, 4);
        }

        /// <summary>
        /// Converts a Liter to Imperial Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Imperial Tablespoon.</returns>
        public static double ToTablespoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 56.3121, 4);
        }

        /// <summary>
        /// Converts a Liter to Imperial Teaspoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Liter converted to Imperial Teaspoon.</returns>
        public static double ToTeaspoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 168.936, 4);
        }
    }
}
