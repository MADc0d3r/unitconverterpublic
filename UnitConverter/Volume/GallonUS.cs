﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Volume
// Class:          GallonUS
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Volume
{
    /// <summary>
    /// Conversions for US Gallon.
    /// </summary>
    public class GallonUS : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "QuartUS":
                    aConvertedValue = ToQuartUS(aValueToConvert);
                    break;
                case "PintUS":
                    aConvertedValue = ToPintUS(aValueToConvert);
                    break;
                case "CupUS":
                    aConvertedValue = ToCupUS(aValueToConvert);
                    break;
                case "OunceUS":
                    aConvertedValue = ToOunceUS(aValueToConvert);
                    break;
                case "TablespoonUS":
                    aConvertedValue = ToTablespoonUS(aValueToConvert);
                    break;
                case "TeaspoonUS":
                    aConvertedValue = ToTeaspoonUS(aValueToConvert);
                    break;
                case "Liter":
                    aConvertedValue = ToLiter(aValueToConvert);
                    break;
                case "Milliliter":
                    aConvertedValue = ToMilliliter(aValueToConvert);
                    break;
                case "GallonImperial":
                    aConvertedValue = ToGallonImperial(aValueToConvert);
                    break;
                case "QuartImperial":
                    aConvertedValue = ToQuartImperial(aValueToConvert);
                    break;
                case "PintImperial":
                    aConvertedValue = ToPintImperial(aValueToConvert);
                    break;
                case "OunceImperial":
                    aConvertedValue = ToOunceImperial(aValueToConvert);
                    break;
                case "TablespoonImperial":
                    aConvertedValue = ToTablespoonImperial(aValueToConvert);
                    break;
                case "TeaspoonImperial":
                    aConvertedValue = ToTeaspoonImperial(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts a US Gallon to US Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to US Quart.</returns>
        public static double ToQuartUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 4, 4);
        }

        /// <summary>
        /// Converts a US Gallon to US Pint
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to US Pint.</returns>
        public static double ToPintUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 8, 4);
        }

        /// <summary>
        /// Converts a US Gallon to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to US Gallon.</returns>
        public static double ToCupUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 16, 4);
        }

        /// <summary>
        /// Converts a US Gallon to US Ounce
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to US Ounce.</returns>
        public static double ToOunceUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 128, 4);
        }

        /// <summary>
        /// Converts a US Gallon to US Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to US Tablespoon.</returns>
        public static double ToTablespoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 256, 4);
        }

        /// <summary>
        /// Converts a US Gallon to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to US Teaspoon.</returns>
        public static double ToTeaspoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 768, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Liter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Liter.</returns>
        public static double ToLiter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3.78541, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Milliliter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Milliliter.</returns>
        public static double ToMilliliter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3785.41, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Imperial Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Imperial Gallon.</returns>
        public static double ToGallonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.832674, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Imperial Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Imperial Quart.</returns>
        public static double ToQuartImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3.3307, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Imperial Pint.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Imperial Pint.</returns>
        public static double ToPintImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 6.66139, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Imperial Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Imperial Ounce.</returns>
        public static double ToOunceImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 133.228, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Imperial Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Imperial Tablespoon.</returns>
        public static double ToTablespoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 213.165, 4);
        }

        /// <summary>
        /// Converts a US Gallon to Imperial Teaspoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Gallon converted to Imperial Teaspoon.</returns>
        public static double ToTeaspoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 639.494, 4);
        }
    }
}
