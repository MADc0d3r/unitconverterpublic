﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Volume
// Class:          PintUS
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Volume
{
    /// <summary>
    /// Conversions for US Pint.
    /// </summary>
    public class PintUS : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "GallonUS":
                    aConvertedValue = ToGallonUS(aValueToConvert);
                    break;
                case "QuartUS":
                    aConvertedValue = ToQuartUS(aValueToConvert);
                    break;
                case "CupUS":
                    aConvertedValue = ToCupUS(aValueToConvert);
                    break;
                case "OunceUS":
                    aConvertedValue = ToOunceUS(aValueToConvert);
                    break;
                case "TablespoonUS":
                    aConvertedValue = ToTablespoonUS(aValueToConvert);
                    break;
                case "TeaspoonUS":
                    aConvertedValue = ToTeaspoonUS(aValueToConvert);
                    break;
                case "Liter":
                    aConvertedValue = ToLiter(aValueToConvert);
                    break;
                case "Milliliter":
                    aConvertedValue = ToMilliliter(aValueToConvert);
                    break;
                case "GallonImperial":
                    aConvertedValue = ToGallonImperial(aValueToConvert);
                    break;
                case "QuartImperial":
                    aConvertedValue = ToQuartImperial(aValueToConvert);
                    break;
                case "PintImperial":
                    aConvertedValue = ToPintImperial(aValueToConvert);
                    break;
                case "OunceImperial":
                    aConvertedValue = ToOunceImperial(aValueToConvert);
                    break;
                case "TablespoonImperial":
                    aConvertedValue = ToTablespoonImperial(aValueToConvert);
                    break;
                case "TeaspoonImperial":
                    aConvertedValue = ToTeaspoonImperial(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts a US Pint to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to US Gallon.</returns>
        public static double ToGallonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.125, 4);
        }

        /// <summary>
        /// Converts a US Pint to US Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to US Quart.</returns>
        public static double ToQuartUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.5, 4);
        }

        /// <summary>
        /// Converts a US Pint to US Cup
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to US Cup.</returns>
        public static double ToCupUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 2, 4);
        }

        /// <summary>
        /// Converts a US Pint to US Ounce
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to US Ounce.</returns>
        public static double ToOunceUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 16, 4);
        }

        /// <summary>
        /// Converts a US Pint to US Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to US Tablespoon.</returns>
        public static double ToTablespoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 32, 4);
        }

        /// <summary>
        /// Converts a US Pint to US Teaspoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to US Teaspoon.</returns>
        public static double ToTeaspoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 96, 4);
        }

        /// <summary>
        /// Converts a US Pint to Liter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Liter.</returns>
        public static double ToLiter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.473176, 4);
        }

        /// <summary>
        /// Converts a US Pint to Milliliter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Milliliter.</returns>
        public static double ToMilliliter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 473.176, 4);
        }

        /// <summary>
        /// Converts a US Pint to Imperial Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Imperial Gallon.</returns>
        public static double ToGallonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.104084, 4);
        }

        /// <summary>
        /// Converts a US Pint to Imperial Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Imperial Quart.</returns>
        public static double ToQuartImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.416337, 4);
        }

        /// <summary>
        /// Converts a US Pint to Imperial Pint.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Imperial Pint.</returns>
        public static double ToPintImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.832674, 4);
        }

        /// <summary>
        /// Converts a US Pint to Imperial Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Imperial Ounce.</returns>
        public static double ToOunceImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 16.6535, 4);
        }

        /// <summary>
        /// Converts a US Pint to Imperial Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Imperial Tablespoon.</returns>
        public static double ToTablespoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 26.6456, 4);
        }

        /// <summary>
        /// Converts a US Pint to Imperial Teaspoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A US Pint converted to Imperial Teaspoon.</returns>
        public static double ToTeaspoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 79.9367, 4);
        }
    }
}
