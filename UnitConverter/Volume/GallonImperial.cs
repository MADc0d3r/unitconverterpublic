﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Volume
// Class:          GallonUS
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/04/2015   Initial Design 
//-----------------------------------------------------------------

using System;

namespace UnitConverter.Volume
{
    /// <summary>
    /// Conversions for GallonImperial.
    /// </summary>
    public class GallonImperial : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "GallonUS":
                    aConvertedValue = ToGallonUS(aValueToConvert);
                    break;
                case "QuartUS":
                    aConvertedValue = ToQuartUS(aValueToConvert);
                    break;
                case "PintUS":
                    aConvertedValue = ToPintUS(aValueToConvert);
                    break;
                case "CupUS":
                    aConvertedValue = ToCupUS(aValueToConvert);
                    break;
                case "OunceUS":
                    aConvertedValue = ToOunceUS(aValueToConvert);
                    break;
                case "TablespoonUS":
                    aConvertedValue = ToTablespoonUS(aValueToConvert);
                    break;
                case "TeaspoonUS":
                    aConvertedValue = ToTeaspoonUS(aValueToConvert);
                    break;
                case "Liter":
                    aConvertedValue = ToLiter(aValueToConvert);
                    break;
                case "Milliliter":
                    aConvertedValue = ToMilliliter(aValueToConvert);
                    break;
                case "QuartImperial":
                    aConvertedValue = ToQuartImperial(aValueToConvert);
                    break;
                case "PintImperial":
                    aConvertedValue = ToPintImperial(aValueToConvert);
                    break;
                case "OunceImperial":
                    aConvertedValue = ToOunceImperial(aValueToConvert);
                    break;
                case "TablespoonImperial":
                    aConvertedValue = ToTablespoonImperial(aValueToConvert);
                    break;
                case "TeaspoonImperial":
                    aConvertedValue = ToTeaspoonImperial(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts a Imperial Gallon to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon converted to US Gallon.</returns>
        public static double ToGallonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1.20095, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to US Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to US Quart.</returns>
        public static double ToQuartUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 4.8038, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to US Pint
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to US Pint.</returns>
        public static double ToPintUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 9.6076, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to US Cup
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Imperial Gallon .</returns>
        public static double ToCupUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 19.2152, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to US Ounce
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to US Ounce.</returns>
        public static double ToOunceUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 153.722, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to US Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to US Tablespoon.</returns>
        public static double ToTablespoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 307.443, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to US Gallon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to US Teaspoon.</returns>
        public static double ToTeaspoonUS(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 922.33, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to Liter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Liter.</returns>
        public static double ToLiter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 4.54609, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to Milliliter
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Milliliter.</returns>
        public static double ToMilliliter(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 4546.09, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to Imperial Quart
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Imperial Quart.</returns>
        public static double ToQuartImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 4, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to Imperial Pint.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Imperial Pint.</returns>
        public static double ToPintImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 8, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to Imperial Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Imperial Ounce.</returns>
        public static double ToOunceImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 160, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to Imperial Tablespoon
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Imperial Tablespoon.</returns>
        public static double ToTablespoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 256, 2);
        }

        /// <summary>
        /// Converts a Imperial Gallon  to Imperial Teaspoon.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>A Imperial Gallon  converted to Imperial Teaspoon.</returns>
        public static double ToTeaspoonImperial(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 768, 2);
        }
    }
}
