﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Mass
// Class:          Ounce
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/07/2015   Initial Design 
//-----------------------------------------------------------------
using System;

namespace UnitConverter.Mass
{
    /// <summary>
    /// Ounce conversion methods.
    /// </summary>
    public class Ounce : IConvert
    {
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Gram":
                    aConvertedValue = ToGram(aValueToConvert);
                    break;
                case "Kilogram":
                    aConvertedValue = ToKilogram(aValueToConvert);
                    break;
                case "Milligram":
                    aConvertedValue = ToMilligram(aValueToConvert);
                    break;
                case "Pound":
                    aConvertedValue = ToPound(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Ounce to Kilogram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Ounce converted to Kilogram.</returns>
        public static double ToKilogram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0283495, 4);
        }

        /// <summary>
        /// Converts Ounce to Gram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Ounce converted to Gram.</returns>
        public static double ToGram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 28.3495, 4);
        }

        /// <summary>
        /// Converts Ounce to Milligram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Ounce converted to Milligram.</returns>
        public static double ToMilligram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 28349.5, 4);
        }

        /// <summary>
        /// Converts Ounce to Pound.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Ounce converted to Pound.</returns>
        public static double ToPound(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.0625, 4);
        }
    }
}
