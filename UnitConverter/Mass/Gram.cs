﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Mass
// Class:          Gram
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/07/2015   Initial Design 
//-----------------------------------------------------------------
using System;

namespace UnitConverter.Mass
{
    /// <summary>
    /// Gram conversion methods.
    /// </summary>
    public class Gram : IConvert
    {
        /// <summary>
        /// Converts a source unit to a destination unit.
        /// </summary>
        /// <param name="aUnitToConvertTo">The unit to convert.</param>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>The converted unit.</returns>
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Kilogram":
                    aConvertedValue = ToKilogram(aValueToConvert);
                    break;
                case "Milligram":
                    aConvertedValue = ToMilligram(aValueToConvert);
                    break;
                case "Ounce":
                    aConvertedValue = ToOunce(aValueToConvert);
                    break;
                case "Pound":
                    aConvertedValue = ToPound(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Gram to Kilogram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Gram converted to Kilogram.</returns>
        public static double ToKilogram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.001, 4);
        }

        /// <summary>
        /// Converts Gram to Milligram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Gram converted to Milligram.</returns>
        public static double ToMilligram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1000, 4);
        }

        /// <summary>
        /// Converts Gram to Pound.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Gram converted to Pound.</returns>
        public static double ToPound(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.00220462, 4);
        }

        /// <summary>
        /// Converts Gram to Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Gram converted to Ounce.</returns>
        public static double ToOunce(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.035274, 4);
        }
    }
}
