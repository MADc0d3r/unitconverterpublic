﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Mass
// Class:          Pound
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/07/2015   Initial Design 
//-----------------------------------------------------------------
using System;

namespace UnitConverter.Mass
{
    /// <summary>
    /// Pound conversion methods.
    /// </summary>
    public class Pound : IConvert
    {
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Gram":
                    aConvertedValue = ToGram(aValueToConvert);
                    break;
                case "Kilogram":
                    aConvertedValue = ToKilogram(aValueToConvert);
                    break;
                case "Milligram":
                    aConvertedValue = ToMilligram(aValueToConvert);
                    break;
                case "Ounce":
                    aConvertedValue = ToOunce(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Pound to Kilogram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Pound converted to Kilogram.</returns>
        public static double ToKilogram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.453592, 4);
        }

        /// <summary>
        /// Converts Pound to Gram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Pound converted to Gram.</returns>
        public static double ToGram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 453.592, 4);
        }

        /// <summary>
        /// Converts Pound to Milligram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Pound converted to Milligram.</returns>
        public static double ToMilligram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 453592, 4);
        }

        /// <summary>
        /// Converts Pound to Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Pound converted to Ounce.</returns>
        public static double ToOunce(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 16, 4);
        }
    }
}
