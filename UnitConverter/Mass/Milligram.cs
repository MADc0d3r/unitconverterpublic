﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Mass
// Class:          Milligram
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/07/2015   Initial Design 
//-----------------------------------------------------------------
using System;

namespace UnitConverter.Mass
{
    /// <summary>
    /// Milligram conversion methods.
    /// </summary>
    public class Milligram : IConvert
    {
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Gram":
                    aConvertedValue = ToGram(aValueToConvert);
                    break;
                case "Kilogram":
                    aConvertedValue = ToKilogram(aValueToConvert);
                    break;
                case "Ounce":
                    aConvertedValue = ToOunce(aValueToConvert);
                    break;
                case "Pound":
                    aConvertedValue = ToPound(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Milligram to Kilogram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Milligram converted to Kilogram.</returns>
        public static double ToKilogram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1e-6, 4);
        }

        /// <summary>
        /// Converts Milligram to Gram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Milligram converted to Gram.</returns>
        public static double ToGram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 0.001, 4);
        }

        /// <summary>
        /// Converts Milligram to Pound.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Milligram converted to Pound.</returns>
        public static double ToPound(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 2.2046e-6, 4);
        }

        /// <summary>
        /// Converts Milligram to Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Milligram converted to Ounce.</returns>
        public static double ToOunce(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 3.5274e-5, 4);
        }
    }
}
