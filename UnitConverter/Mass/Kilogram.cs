﻿//-----------------------------------------------------------------
// Namespace:      UnitConverter.Mass
// Class:          Kilogram
// Description:    Conversion Utility Methods.
// Author:         Michael A. Diaz
// 
// Name:            Date:        Description:
// -------------    -----------  ---------------------------------
// M.Diaz           03/07/2015   Initial Design 
//-----------------------------------------------------------------
using System;

namespace UnitConverter.Mass
{
    /// <summary>
    /// Kilogram conversion methods.
    /// </summary>
    public class Kilogram : IConvert
    {
        public double Convert(string aUnitToConvertTo, double aValueToConvert)
        {
            double aConvertedValue = 0.0;

            switch (aUnitToConvertTo)
            {
                case "Gram":
                    aConvertedValue = ToGram(aValueToConvert);
                    break;
                case "Milligram":
                    aConvertedValue = ToMilligram(aValueToConvert);
                    break;
                case "Ounce":
                    aConvertedValue = ToOunce(aValueToConvert);
                    break;
                case "Pound":
                    aConvertedValue = ToPound(aValueToConvert);
                    break;
            }

            return aConvertedValue;
        }

        /// <summary>
        /// Converts Kilogram to Gram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Kilogram converted to Gram.</returns>
        public static double ToGram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1000, 4);
        }

        /// <summary>
        /// Converts Kilogram to Milligram.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Kilogram converted to Milligram.</returns>
        public static double ToMilligram(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 1e+6, 4);
        }

        /// <summary>
        /// Converts Kilogram to Pound.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Kilogram converted to Pound.</returns>
        public static double ToPound(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 2.20462, 4);
        }

        /// <summary>
        /// Converts Kilogram to Ounce.
        /// </summary>
        /// <param name="aValueToConvert">The value to convert.</param>
        /// <returns>Kilogram converted to Ounce.</returns>
        public static double ToOunce(double aValueToConvert)
        {
            return Math.Round(aValueToConvert * 35.274, 4);
        }
    }
}
